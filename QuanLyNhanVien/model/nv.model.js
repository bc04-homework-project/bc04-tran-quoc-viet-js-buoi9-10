function NhanVien(
  taiKhoan,
  ten,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLamTrongThang
) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLamTrongThang = gioLamTrongThang;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCoBan * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCoBan * 2;
    } else {
      return this.luongCoBan * 1;
    }
  };
  this.loaiNhanVien = function () {
    if (this.gioLamTrongThang >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gioLamTrongThang >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gioLamTrongThang >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
};

//data to test
function dataTotest(num) {
  for (let n = 1; n <= num; n++) {
    let nv = new NhanVien(
      "acc" + n,
      "Tran Van " + String.fromCharCode(65 + n),
      "mail@g.co" + n,
      "abcDEF@!" + n,
      `08/22/2021`,
      n < 20 ? n * 1e6 : 2e7,
      n < num - 2 ? "Nhân viên" : n < num ? "Trưởng phòng" : "Sếp",
      n < 12 ? 80 + n * 10 : 200
    );
    dsnv.push(nv);
  }
}
