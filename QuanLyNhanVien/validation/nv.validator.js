//Quy tắc kiểm tra
var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraGiaTri:  function (value, idError, message, min, max) {
    if (value < min || value > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraChu: function (value, idError, message) {
    if(!(/^[a-zA-Z, ]+$/.test(value))){
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  
  kiemTraLoai:  function (value, idError, message) {
    if (value == "Sếp" || value == 'Trưởng phòng' || value == 'Nhân viên' ) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

  kiemTraMatKhau: function (value, idError, message) {
    const repw = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#^.,])[A-Za-z\d@$!%*?&#^.,]{6,10}$/;

    if (repw.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },

};

//Các giá trị kiểm tra
function isValid(nv) {

  var isValid =
    validator.kiemTraRong(nv.taiKhoan, "tbTKNV", "Tài khoản không được rỗng") &&
    validator.kiemTraDoDai(
      nv.taiKhoan,
      "tbTKNV",
      "Tài khoản nhân viên phải có từ 4-6 ký tự",
      4,
      6
    );

    isValid =
    isValid &
    validator.kiemTraRong(
      nv.ten,
      "tbTen",
      "Tên nhân viên không được rỗng"
    ) &&  validator.kiemTraChu(
      nv.ten,
      "tbTen",
      "Tên nhân viên phải là chữ không dấu và không chứa số"
    ) ;

  isValid =
    isValid &
      validator.kiemTraRong(nv.email, "tbEmail", "Email không được rỗng") &&
    validator.kiemTraEmail(nv.email, "tbEmail", "Email không hợp lệ");

    isValid =
    isValid & validator.kiemTraMatKhau(
      nv.matKhau,
      "tbMatKhau",
      "Mật khẩu phải dài từ 6-10 ký tự, trong đó có ít nhất 1 ký tự viết thường, 1 ký tự viết hoa, 1 ký tự đặc biệt và 1 chữ số"
    ) ;
    
    isValid =
    isValid &
    validator.kiemTraRong(nv.ngayLam, "tbNgay", "Ngày làm không được rỗng") ;
    
    isValid =
    isValid &
    validator.kiemTraRong(nv.luongCoBan, "tbLuongCB", "Tiền lương không được rỗng") && validator.kiemTraGiaTri(nv.luongCoBan, "tbLuongCB", "Tiền lương phải từ 1 triệu - 20 triệu", 1e6, 2e7) ;

    isValid =
    isValid &
    validator.kiemTraLoai(
      nv.chucVu,
      "tbChucVu",
      "Chưa chọn chức vụ"
    ) ;
      
    isValid =
    isValid &
    validator.kiemTraRong(nv.gioLamTrongThang, "tbGiolam", "Giờ làm không được rỗng") && validator.kiemTraGiaTri(nv.gioLamTrongThang, "tbGiolam", "Giờ làm trong tháng phải từ 80-200 giờ", 80, 200);

  return isValid;
}
