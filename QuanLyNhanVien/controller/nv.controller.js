function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let ten = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let matKhau = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luongCoBan = document.getElementById("luongCB").value;
  let chucVu = document.getElementById("chucvu").value;
  let gioLamTrongThang = document.getElementById("gioLam").value;

  return new NhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLamTrongThang
  );
}

function renderDSNV(dsnv) {
  // console.log("dsnv: ", dsnv);
  var contentHTML = "";

  dsnv.forEach((nv) => {
    var trContent = ` <tr> 
          <td>${nv.taiKhoan}</td>
          <td>${nv.ten}</td>
          <td>${nv.email}</td>
          <td>${nv.ngayLam}</td>
          <td>${nv.chucVu}</td>
          <td>${nv.tongLuong()}</td>
          <td>${nv.loaiNhanVien()}</td>

          <td>
          <button onclick="xoaNV('${
            nv.taiKhoan
          }')" class="btn btn-danger">Xóa</button>
<button onclick="suaNV('${
      nv.taiKhoan
    }')" class="btn btn-warning" data-toggle="modal"
data-target="#myModal">Sửa</button>
</td>
          </tr>
          `;
    contentHTML += trContent;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLamTrongThang;
}

function timKiemViTri(account, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == account) {
      return index;
    }
  }
  return -1;
}

function searchNV() {
  let timNV = document.getElementById("searchName").value;

  let nvXuatSac = [];
  let nvGioi = [];
  let nvKha = [];
  let nvTB = [];

  dsnv.forEach((nv) => {
    if (nv.loaiNhanVien() == "Nhân viên xuất sắc") {
      nvXuatSac.push(nv);
    } else if (nv.loaiNhanVien() == "Nhân viên giỏi") {
      nvGioi.push(nv);
    } else if (nv.loaiNhanVien() == "Nhân viên khá") {
      nvKha.push(nv);
    } else {
      nvTB.push(nv);
    }
  });

  ["xuat sac", "xuất sắc"].includes(timNV.toLowerCase())
    ? renderDSNV(nvXuatSac)
    : ["gioi", "giỏi"].includes(timNV.toLowerCase())
    ? renderDSNV(nvGioi)
    : ["kha", "khá"].includes(timNV.toLowerCase())
    ? renderDSNV(nvKha)
    : ["trung binh", "trung bình"].includes(timNV.toLowerCase())
    ? renderDSNV(nvTB)
    : (document.getElementById(
        "tableDanhSach"
      ).innerHTML = `Không tìm thấy thông tin bạn nhập <br> Loại nhân viên chỉ bao gồm Xuất sắc / Giỏi / Khá / Trung Bình`);
}

document
  .getElementById("btnDong")
  .addEventListener("click", function dongForm() {
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnThemNV").disabled = false;
  });
