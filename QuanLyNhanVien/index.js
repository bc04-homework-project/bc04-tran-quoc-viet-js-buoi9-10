const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

var dsnv = [];
dataTotest(12); //set sẵn một ít data để đỡ phải nhập tay lần đầu
renderDSNV(dsnv);

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);

  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];

    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCoBan,
      nv.chucVu,
      nv.gioLamTrongThang
    );
  }
  renderDSNV(dsnv);
}

document
  .getElementById("btnThemNV")
  .addEventListener("click", function themNV() {
    var newNV = layThongTinTuForm();
    // console.log("newNV: ", newNV);

    var isValid1 = isValid(newNV);
    if (isValid1) {
      dsnv.push(newNV);

      var dsnvJson = JSON.stringify(dsnv);
      localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    }

    renderDSNV(dsnv);
  });

function xoaNV(account) {
  var index = timKiemViTri(account, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    dsnv.splice(index, 1);

    localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));

    renderDSNV(dsnv);
  }
}

function suaNV(account) {
  var index = timKiemViTri(account, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
}

document
  .getElementById("btnCapNhat")
  .addEventListener("click", function capNhatNV() {
    var fixNV = layThongTinTuForm();
    var isValid1 = isValid(fixNV);
    var index = timKiemViTri(fixNV.taiKhoan, dsnv);
    if (isValid1) {
      dsnv[index] = fixNV;

      localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));
    }

    renderDSNV(dsnv);
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnThemNV").disabled = false;
  });

//search nhân viên theo xếp loại
document.getElementById("btnTimNV").addEventListener("click", () => {
  searchNV();
});
document.getElementById("searchName").addEventListener("keydown", (e) => {
  if (e.key === "Enter") {
    searchNV();
  }
});
